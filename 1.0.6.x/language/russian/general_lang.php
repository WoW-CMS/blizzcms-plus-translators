<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Browser Tab Menu*/
$lang['tab_news'] = 'Новости';
$lang['tab_forum'] = 'Форум';
$lang['tab_store'] = 'Магазин';
$lang['tab_bugtracker'] = 'Баг трекер';
$lang['tab_changelogs'] = 'Список изменений';
$lang['tab_pvp_statistics'] = 'Статистсика PvP';
$lang['tab_login'] = 'Авторизация';
$lang['tab_register'] = 'Регистрация';
$lang['tab_home'] = 'Главная';
$lang['tab_donate'] = 'Пожертвование';
$lang['tab_vote'] = 'Голосование';
$lang['tab_cart'] = 'Корзина';
$lang['tab_account'] = 'Моя учётная запись';
$lang['tab_reset'] = 'Восстановление пароля';
$lang['tab_error'] = 'Ошибка 404';
$lang['tab_maintenance'] = 'Техническое обслуживание';
$lang['tab_online'] = 'Игроки онлайн';

/*Panel Navbar*/
$lang['navbar_vote_panel'] = 'Панель голосования';
$lang['navbar_donate_panel'] = 'Панель пожертвования';

/*Button Lang*/
$lang['button_register'] = 'Регистрация';
$lang['button_login'] = 'Авторизоваться';
$lang['button_logout'] = 'Выйти';
$lang['button_forgot_password'] = 'Забыли свой пароль?';
$lang['button_user_panel'] = 'Панель пользователя';
$lang['button_admin_panel'] = 'Панель администратора';
$lang['button_mod_panel'] = 'Панель модератора';
$lang['button_change_avatar'] = 'Изменить аватар';
$lang['button_add_personal_info'] = 'Добавить персональную информацию';
$lang['button_create_report'] = 'Создать отчёт';
$lang['button_new_topic'] = 'Новая тема';
$lang['button_edit_topic'] = 'Редактировать тему';
$lang['button_save_changes'] = 'Сохранить изменения';
$lang['button_cancel'] = 'Отмена';
$lang['button_send'] = 'Отправить';
$lang['button_read_more'] = 'Подробнее';
$lang['button_add_reply'] = 'Добавить ответ';
$lang['button_remove'] = 'Удалить';
$lang['button_create'] = 'Создать';
$lang['button_save'] = 'Сохранить';
$lang['button_close'] = 'Закрыть';
$lang['button_reply'] = 'Ответ';
$lang['button_donate'] = 'Пожертвовать';
$lang['button_account_settings'] = 'Настройки учётной записи';
$lang['button_cart'] = 'Добавить в корзину';
$lang['button_view_cart'] = 'Просмотр корзины';
$lang['button_checkout'] = 'Приобрести';
$lang['button_buying'] = 'Продолжить покупки';

/*Alert Lang*/
$lang['alert_successful_purchase'] = 'Товар успешно куплен.';
$lang['alert_upload_error'] = 'Ваше изображение должно быть в формате jpg или png';
$lang['alert_changelog_not_found'] = 'В данный момент на сервере нет списков изменений';
$lang['alert_points_insufficient'] = 'Недостаточно очков';

/*Status Lang*/
$lang['offline'] = 'Выключен';
$lang['online'] = 'Включен';

/*Label Lang*/
$lang['label_open'] = 'Открыт';
$lang['label_closed'] = 'Закрыт';

/*Form Label Lang*/
$lang['label_login_info'] = 'Информация для входа';

/*Input Placeholder Lang*/
$lang['placeholder_username'] = 'Имя пользователя';
$lang['placeholder_email'] = 'Адрес электронной почты';
$lang['placeholder_password'] = 'Пароль';
$lang['placeholder_re_password'] = 'Повторите пароль';
$lang['placeholder_current_password'] = 'Текущий пароль';
$lang['placeholder_new_password'] = 'Новый пароль';
$lang['placeholder_new_email'] = 'Новая почта';
$lang['placeholder_confirm_email'] = 'Подтвердите новую почту';
$lang['placeholder_create_bug_report'] = 'Создать отчёт об ошибке';
$lang['placeholder_title'] = 'Заголовок';
$lang['placeholder_type'] = 'Тип';
$lang['placeholder_description'] = 'Описание';
$lang['placeholder_url'] = 'URL';
$lang['placeholder_uri'] = 'Дружественный URL (Пример: tos)';
$lang['placeholder_highl'] = 'Закрепить';
$lang['placeholder_lock'] = 'Заблокировать';
$lang['placeholder_subject'] = 'Тема';

/*Table header Lang*/
$lang['table_header_name'] = 'Имя';
$lang['table_header_faction'] = 'Фракция';
$lang['table_header_total_kills'] = 'Всего убийств';
$lang['table_header_today_kills'] = 'Сегодняшние убийства';
$lang['table_header_yersterday_kills'] = 'Вчерашние убийства';
$lang['table_header_team_name'] = 'Имя команды';
$lang['table_header_members'] = 'Участники';
$lang['table_header_rating'] = 'Рейтинг';
$lang['table_header_games'] = 'Игры';
$lang['table_header_id'] = 'ID';
$lang['table_header_status'] = 'Статус';
$lang['table_header_priority'] = 'Приоритет';
$lang['table_header_date'] = 'Дата';
$lang['table_header_author'] = 'Автор';
$lang['table_header_time'] = 'Время';
$lang['table_header_icon'] = 'Иконка';
$lang['table_header_realm'] = 'Мир';
$lang['table_header_zone'] = 'Зона';
$lang['table_header_character'] = 'Персонаж';
$lang['table_header_price'] = 'Цена';
$lang['table_header_item_name'] = 'Название предмета';
$lang['table_header_items'] = 'Предмет(ы)';
$lang['table_header_quantity'] = 'Количество';

/*Class Lang*/
$lang['class_warrior'] = 'Воин';
$lang['class_paladin'] = 'Паладин';
$lang['class_hunter'] = 'Охотник';
$lang['class_rogue'] = 'Разбойник';
$lang['class_priest'] = 'Жрец';
$lang['class_dk'] = 'Рыцарь смерти';
$lang['class_shamman'] = 'Шаман';
$lang['class_mage'] = 'Маг';
$lang['class_warlock'] = 'Чернокнижник';
$lang['class_monk'] = 'Монах';
$lang['class_druid'] = 'Друид';
$lang['class_demonhunter'] = 'Охотник на демонов';

/*Faction Lang*/
$lang['faction_alliance'] = 'Альянс';
$lang['faction_horde'] = 'Орда';

/*Gender Lang*/
$lang['gender_male'] = 'Мужчина';
$lang['gender_female'] = 'Женщина';

/*Race Lang*/
$lang['race_human'] = 'Человек';
$lang['race_orc'] = 'Орк';
$lang['race_dwarf'] = 'Дворф';
$lang['race_night_elf'] = 'Ночной эльф';
$lang['race_undead'] = 'Нежить';
$lang['race_tauren'] = 'Таурен';
$lang['race_gnome'] = 'Гном';
$lang['race_troll'] = 'Тролль';
$lang['race_goblin'] = 'Гоблин';
$lang['race_blood_elf'] = 'Эльф крови';
$lang['race_draenei'] = 'Дреней';
$lang['race_worgen'] = 'Ворген';
$lang['race_panda_neutral'] = 'Пандарен нейтральный';
$lang['race_panda_alli'] = 'Пандарен Альянса';
$lang['race_panda_horde'] = 'Пандарен Орды';
$lang['race_nightborde'] = 'Ночнорожденный';
$lang['race_void_elf'] = 'Эльф Бездны';
$lang['race_lightforged_draenei'] = 'Озаренный дреней';
$lang['race_highmountain_tauren'] = 'Таурен Крутогорья';
$lang['race_dark_iron_dwarf'] = 'Дворф из клана Черного Железа';
$lang['race_maghar_orc'] = 'Маг`хар';

/*Header Lang*/
$lang['header_cookie_message'] = 'Этот веб-сайт использует куки-файлы, чтобы обеспечить вам максимальный комфорт на нашем веб-сайте. ';
$lang['header_cookie_button'] = 'Понял!';

/*Footer Lang*/
$lang['footer_rights'] = 'Все права защищены.';

/*Page 404 Lang*/
$lang['page_404_title'] = '404 Страница не найдена';
$lang['page_404_description'] = 'Похоже, что страница, которую вы ищете, не может быть найдена';

/*Panel Lang*/
$lang['panel_acc_rank'] = 'Ранг учётной записи';
$lang['panel_dp'] = 'DP';
$lang['panel_vp'] = 'VP';
$lang['panel_expansion'] = 'Дополнение';
$lang['panel_member'] = 'Участник с';
$lang['panel_chars_list'] = 'Список персонажей';
$lang['panel_account_details'] = 'Детали учётной записи';
$lang['panel_last_ip'] = 'Последний IP';
$lang['panel_change_email'] = 'Изменить адрес почты';
$lang['panel_change_password'] = 'Изменить пароль';
$lang['panel_replace_pass_by'] = 'Изменить пароль на';
$lang['panel_current_email'] = 'Текущий адрес почты';
$lang['panel_replace_email_by'] = 'Изменить почту на';

/*Home Lang*/
$lang['home_latest_news'] = 'Последние новости';
$lang['home_discord'] = 'Discord';
$lang['home_server_status'] = 'Статус сервера';
$lang['home_realm_info'] = 'Текущий мир:';

/*PvP Statistics Lang*/
$lang['statistics_top_20'] = 'ТОП 20';
$lang['statistics_top_2v2'] = 'ТОП 2V2';
$lang['statistics_top_3v3'] = 'ТОП 3V3';
$lang['statistics_top_5v5'] = 'ТОП 5V5';

/*News Lang*/
$lang['news_recent_list'] = 'Список последних новостей';
$lang['news_comments'] = 'Комментарии';

/*Bugtracker Lang*/
$lang['bugtracker_report_notfound'] = 'Отчёты не найдены';

/*Donate Lang*/
$lang['donate_get'] = 'Получить';

/*Vote Lang*/
$lang['vote_next_time'] = 'Следующее голосование в:';

/*Forum Lang*/
$lang['forum_posts_count'] = 'Сообщения';
$lang['forum_topic_locked'] = 'Данная тема заблокирована.';
$lang['forum_comment_locked'] = 'Есть что сказать? Авторизуйтесь, чтобы присоединиться к разговору.';
$lang['forum_comment_header'] = 'Присоединиться к разговору';
$lang['forum_not_authorized'] = 'Не авторизован';
$lang['forum_post_history'] = 'Посмотреть историю сообщений';
$lang['forum_topic_list'] = 'Список тем';
$lang['forum_last_activity'] = 'Последняя активность';
$lang['forum_last_post_by'] = 'Последнее сообщение от';
$lang['forum_whos_online'] = 'Кто онлайн';
$lang['forum_replies_count'] = 'Ответы';
$lang['forum_topics_count'] = 'Темы';
$lang['forum_users_count'] = 'Пользователи';

/*Store Lang*/
$lang['store_categories'] = 'Категрии магазина';
$lang['store_top_items'] = 'Топ предметов';
$lang['store_cart_added'] = 'Вы добавили';
$lang['store_cart_in_your'] = 'в свою корзину';
$lang['store_cart_no_items'] = 'У вас нет товаров в вашей корзине.';

/*Soap Lang*/
$lang['soap_send_subject'] = 'Магазин';
$lang['soap_send_body'] = 'Спасибо за покупку в нашем магазине!';

/*Email Lang*/
$lang['email_password_recovery'] = 'Восстановление пароля';
$lang['email_account_activation'] = 'Активация учётной записи';
