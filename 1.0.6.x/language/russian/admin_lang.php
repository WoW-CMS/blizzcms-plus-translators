<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Navbar Lang*/
$lang['admin_nav_dashboard'] = 'Панель счётчиков';
$lang['admin_nav_system'] = 'Система';
$lang['admin_nav_manage_settings'] = 'Управление настройками';
$lang['admin_nav_manage_modules'] = 'Управление модулями';
$lang['admin_nav_users'] = 'Пользователи';
$lang['admin_nav_accounts'] = 'Учётные записи';
$lang['admin_nav_website'] = 'Сайт';
$lang['admin_nav_menu'] = 'Меню';
$lang['admin_nav_realms'] = 'Миры';
$lang['admin_nav_slides'] = 'Слайды';
$lang['admin_nav_news'] = 'Новости';
$lang['admin_nav_changelogs'] = 'Список изменений';
$lang['admin_nav_pages'] = 'Страницы';
$lang['admin_nav_donate_methods'] = 'Способы пожертвования';
$lang['admin_nav_topsites'] = 'Топы';
$lang['admin_nav_donate_vote_logs'] = 'Логи пожертвований/голосований';
$lang['admin_nav_store'] = 'Магазин';
$lang['admin_nav_manage_store'] = 'Управление магазином';
$lang['admin_nav_forum'] = 'Форум';
$lang['admin_nav_manage_forum'] = 'Управление форумом';
$lang['admin_nav_logs'] = 'Система логирования';

/*Sections Lang*/
$lang['section_general_settings'] = 'Общие настройки';
$lang['section_module_settings'] = 'Настройки модулей';
$lang['section_optional_settings'] = 'Дополнительные настройки';
$lang['section_seo_settings'] = 'SEO настройки';
$lang['section_update_cms'] = 'Обновить CMS';
$lang['section_check_information'] = 'Проверка информации';
$lang['section_forum_categories'] = 'Категории форума';
$lang['section_forum_elements'] = 'Элементы форума';
$lang['section_store_categories'] = 'Категории магазина';
$lang['section_store_items'] = 'Магазин предметов';
$lang['section_store_top'] = 'Топ предметов магазина';
$lang['section_logs_dp'] = 'Логи пожертвований';
$lang['section_logs_vp'] = 'Логи голосований';

/*Button Lang*/
$lang['button_select'] = 'Выбрать';
$lang['button_update'] = 'Обновить';
$lang['button_unban'] = 'Разблокировать';
$lang['button_ban'] = 'Заблокировать';
$lang['button_remove'] = 'Снять';
$lang['button_grant'] = 'Выдать';
$lang['button_update_version'] = 'Обновить до последней версии';

/*Table header Lang*/
$lang['table_header_subcategory'] = 'Выберите подкатегорию';
$lang['table_header_race'] = 'Раса';
$lang['table_header_class'] = 'Класс';
$lang['table_header_level'] = 'Уровень';
$lang['table_header_money'] = 'Деньги';
$lang['table_header_time_played'] = 'Время в игре';
$lang['table_header_actions'] = 'Действия';
$lang['table_header_id'] = '#ID';
$lang['table_header_tax'] = 'Пошлина';
$lang['table_header_points'] = 'Очки';
$lang['table_header_type'] = 'Тип';
$lang['table_header_module'] = 'Модуль';
$lang['table_header_payment_id'] = 'ID платежа';
$lang['table_header_hash'] = 'Хэш';
$lang['table_header_total'] = 'Всего';
$lang['table_header_create_time'] = 'Время создания';
$lang['table_header_guid'] = 'Guid';
$lang['table_header_information'] = 'Информация';
$lang['table_header_value'] = 'Значение';

/*Input Placeholder Lang*/
$lang['placeholder_manage_account'] = 'Управление учётной записи';
$lang['placeholder_update_information'] = 'Обновить информацию учётной записи';
$lang['placeholder_donation_logs'] = 'Логи пожертвований';
$lang['placeholder_store_logs'] = 'Логи магазина';
$lang['placeholder_create_changelog'] = 'Создание списка изменений';
$lang['placeholder_edit_changelog'] = 'Редактировать список изменений';
$lang['placeholder_create_category'] = 'Создать категорию';
$lang['placeholder_edit_category'] = 'Редактировать категорию';
$lang['placeholder_create_forum'] = 'Создать форум';
$lang['placeholder_edit_forum'] = 'Редактировать форум';
$lang['placeholder_create_menu'] = 'Создать меню';
$lang['placeholder_edit_menu'] = 'Редактировать меню';
$lang['placeholder_create_news'] = 'Создать новость';
$lang['placeholder_edit_news'] = 'Редактировать новость';
$lang['placeholder_create_page'] = 'Создать страницу';
$lang['placeholder_edit_page'] = 'Редактировать страницу';
$lang['placeholder_create_realm'] = 'Создать мир';
$lang['placeholder_edit_realm'] = 'Редактировать мир';
$lang['placeholder_create_slide'] = 'Создать слайд';
$lang['placeholder_edit_slide'] = 'Реадактировать слайд';
$lang['placeholder_create_item'] = 'Создать предмет';
$lang['placeholder_edit_item'] = 'Редактировать предмет';
$lang['placeholder_create_topsite'] = 'Создать топ';
$lang['placeholder_edit_topsite'] = 'Редактировать топ';
$lang['placeholder_create_top'] = 'Добавить ТОП предмет';
$lang['placeholder_edit_top'] = 'Редактировать ТОП предмет';

$lang['placeholder_upload_image'] = 'Загрузить изображение';
$lang['placeholder_icon_name'] = 'Название иконки';
$lang['placeholder_category'] = 'Категория';
$lang['placeholder_name'] = 'Название';
$lang['placeholder_item'] = 'Предмет';
$lang['placeholder_image_name'] = 'Название изображения';
$lang['placeholder_reason'] = 'Причина';
$lang['placeholder_gmlevel'] = 'GM уровень';
$lang['placeholder_url'] = 'URL';
$lang['placeholder_child_menu'] = 'Родительское меню';
$lang['placeholder_url_type'] = 'Тип URL';
$lang['placeholder_route'] = 'Маршрут';
$lang['placeholder_hours'] = 'Часы';
$lang['placeholder_soap_hostname'] = 'Хост SOAP';
$lang['placeholder_soap_port'] = 'Порт SOAP';
$lang['placeholder_soap_user'] = 'Пользователь SOAP';
$lang['placeholder_soap_password'] = 'Пароль SOAP';
$lang['placeholder_db_character'] = 'Персонаж';
$lang['placeholder_db_hostname'] = 'Хост базы данных';
$lang['placeholder_db_name'] = 'Имя базы данных';
$lang['placeholder_db_user'] = 'Пользователь базы данных';
$lang['placeholder_db_password'] = 'Пароль базы данных';
$lang['placeholder_account_points'] = 'Очки учётной записи';
$lang['placeholder_account_ban'] = 'Заблокировать учётную запись';
$lang['placeholder_account_unban'] = 'Разблокировать учётную запись';
$lang['placeholder_account_grant_rank'] = 'Выдать GM ранг';
$lang['placeholder_account_remove_rank'] = 'Снять GM ранг';
$lang['placeholder_command'] = 'Команда';

/*Config Lang*/
$lang['conf_website_name'] = 'Название сайта';
$lang['conf_realmlist'] = 'Realmlist';
$lang['conf_discord_invid'] = 'ID приглашения Discord';
$lang['conf_timezone'] = 'Временная зона';
$lang['conf_theme_name'] = 'Название темы';
$lang['conf_maintenance_mode'] = 'Режим технического обслуживания';
$lang['conf_social_facebook'] = 'Facebook URL';
$lang['conf_social_twitter'] = 'Twitter URL';
$lang['conf_social_youtube'] = 'Youtube URL';
$lang['conf_paypal_currency'] = 'Валюта PayPal';
$lang['conf_paypal_mode'] = 'Мод PayPal';
$lang['conf_paypal_client'] = 'ID клиента PayPal';
$lang['conf_paypal_secretpass'] = 'Секретный пароль PayPal';
$lang['conf_default_description'] = 'Описание по умолчанию';
$lang['conf_admin_gmlvl'] = 'GM уровень Администратора';
$lang['conf_mod_gmlvl'] = 'GM уровень Модератора';
$lang['conf_recaptcha_key'] = 'Ключ сайта reCaptcha';
$lang['conf_account_activation'] = 'Активация учётной записи';
$lang['conf_smtp_hostname'] = 'Хост SMTP';
$lang['conf_smtp_port'] = 'Порт SMTP';
$lang['conf_smtp_encryption'] = 'Шифрование SMTP';
$lang['conf_smtp_username'] = 'Пользователь SMTP';
$lang['conf_smtp_password'] = 'Пароль SMTP';
$lang['conf_sender_email'] = 'Email отправителя';
$lang['conf_sender_name'] = 'Имя отправителя';

/*Logs */
$lang['placeholder_logs_dp'] = 'Пожертвование';
$lang['placeholder_logs_quantity'] = 'Количество';
$lang['placeholder_logs_hash'] = 'Хэш';
$lang['placeholder_logs_voteid'] = 'ID голоса';
$lang['placeholder_logs_points'] = 'Очки';
$lang['placeholder_logs_lasttime'] = 'Последний раз';
$lang['placeholder_logs_expiredtime'] = 'Время истекло';

/*Status Lang*/
$lang['status_completed'] = 'Завершено';
$lang['status_cancelled'] = 'Отменено';

/*Options Lang*/
$lang['option_normal'] = 'Стандартное';
$lang['option_dropdown'] = 'Выпадающие';
$lang['option_image'] = 'Изображение';
$lang['option_video'] = 'Видео';
$lang['option_iframe'] = 'Iframe';
$lang['option_enabled'] = 'Включено';
$lang['option_disabled'] = 'Отключено';
$lang['option_ssl'] = 'SSL';
$lang['option_tls'] = 'TLS';
$lang['option_everyone'] = 'Все';
$lang['option_staff'] = 'Персонал';
$lang['option_all'] = 'Персонал - Все';
$lang['option_rename'] = 'Изменинить имя';
$lang['option_customize'] = 'Изменить внешность';
$lang['option_change_faction'] = 'Изменить фракцию';
$lang['option_change_race'] = 'Изменить внешность';
$lang['option_dp'] = 'DP';
$lang['option_vp'] = 'VP';
$lang['option_dp_vp'] = 'DP и VP';
$lang['option_internal_url'] = 'Внутренний URL';
$lang['option_external_url'] = 'Внешний URL';
$lang['option_on'] = 'Вкл';
$lang['option_off'] = 'Выкл';

/*Count Lang*/
$lang['count_accounts_created'] = 'Создано учётных записей';
$lang['count_accounts_banned'] = 'Заблокировано учётных записей';
$lang['count_news_created'] = 'Создано новостей';
$lang['count_changelogs_created'] = 'Создано списков изменений';
$lang['total_accounts_registered'] = 'Общее количество зарегистрированных учётных записей.';
$lang['total_accounts_banned'] = 'Общее количество заблокированных учётных записей.';
$lang['total_news_writed'] = 'Общее количество созданных новостей.';
$lang['total_changelogs_writed'] = 'Общее количество написанных списков изменений.';
$lang['info_alliance_players'] = 'Игроки Альянса';
$lang['info_alliance_playing'] = 'Играющие игроки Альянса';
$lang['info_horde_players'] = 'Игроки Орды';
$lang['info_horde_playing'] = 'Играющие игроки Орды';
$lang['info_players_playing'] = 'Всего игроков в мире';

/*Alert Lang*/
$lang['alert_smtp_activation'] = 'Если включена данная опция, вы должны настроить SMTP для отправки писем.';
$lang['alert_banned_reason'] = 'Заблокирован, причина:';

/*Logs Lang*/
$lang['log_new_level'] = 'Получите новый уровень';
$lang['log_old_level'] = 'До этого было';
$lang['log_new_name'] = 'У него новое имя';
$lang['log_old_name'] = 'До этого было';
$lang['log_unbanned'] = 'Разблокирован';
$lang['log_customization'] = 'Получите изменение внешнего вида';
$lang['log_change_race'] = 'Получите изменение расы';
$lang['log_change_faction'] = 'Получите изменение фракции';
$lang['log_banned'] = 'Был заблокирован';
$lang['log_gm_assigned'] = 'Получил GM ранг';
$lang['log_gm_removed'] = 'GM ранг был удалён';

/*CMS Lang*/
$lang['cms_version_currently'] = 'Эта версия в настоящее время запущена';
$lang['cms_warning_update'] = 'При обновлении CMS конфигурация может быть восстановлена по умолчанию в зависимости от изменений, внесенных в каждую версию.';
$lang['cms_php_version'] = 'Версия PHP';
$lang['cms_allow_fopen'] = 'allow_url_fopen';
$lang['cms_allow_include'] = 'allow_url_include';
$lang['cms_loaded_modules'] = 'Загруженные модули';
$lang['cms_loaded_extensions'] = 'Загруженные расширения';
