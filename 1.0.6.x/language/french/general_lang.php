<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Browser Tab Menu*/
$lang['tab_news'] = 'News';
$lang['tab_forum'] = 'Forum';
$lang['tab_store'] = 'Boutique';
$lang['tab_bugtracker'] = 'Bugtracker';
$lang['tab_changelogs'] = 'Changelogs';
$lang['tab_pvp_statistics'] = 'Statistiques PvP';
$lang['tab_login'] = 'S\'identifier';
$lang['tab_register'] = 'S\'inscrire';
$lang['tab_home'] = 'Accueil';
$lang['tab_donate'] = 'Faire un don';
$lang['tab_vote'] = 'Vote';
$lang['tab_cart'] = 'Panier';
$lang['tab_account'] = 'Mon compte';
$lang['tab_reset'] = 'Récupération de mot de passe';
$lang['tab_error'] = 'Erreur 404';
$lang['tab_maintenance'] = 'Maintenance';
$lang['tab_online'] = 'Joueurs en ligne';

/*Panel Navbar*/
$lang['navbar_vote_panel'] = 'Panel de Vote';
$lang['navbar_donate_panel'] = 'Panel Donation';

/*Button Lang*/
$lang['button_register'] = 'S\'inscrire';
$lang['button_login'] = 'S\'identifier';
$lang['button_logout'] = 'Se déconnecter';
$lang['button_forgot_password'] = 'Mot de passe oublié?';
$lang['button_user_panel'] = 'Panneau d\'Utilisateur';
$lang['button_admin_panel'] = 'Panneau d\'Administration';
$lang['button_mod_panel'] = 'Panneau Modérateur';
$lang['button_change_avatar'] = 'Changer d\'avatar';
$lang['button_add_personal_info'] = 'Ajouter des informations personnelles';
$lang['button_create_report'] = 'Creer un rapport';
$lang['button_new_topic'] = 'Nouveau Topic';
$lang['button_edit_topic'] = 'Editer Topic';
$lang['button_save_changes'] = 'Sauvegarder les modifications';
$lang['button_cancel'] = 'Annuler';
$lang['button_send'] = 'Envoyer';
$lang['button_read_more'] = 'Lire la suite';
$lang['button_add_reply'] = 'Ajouter une réponse';
$lang['button_remove'] = 'Supprimer';
$lang['button_create'] = 'Créer';
$lang['button_save'] = 'Sauvegarder';
$lang['button_close'] = 'Fermer';
$lang['button_reply'] = 'Répondre';
$lang['button_donate'] = 'Faire un don';
$lang['button_account_settings'] = 'Paramètres du compte';
$lang['button_cart'] = 'Ajouter au panier';
$lang['button_view_cart'] = 'Voir le panier';
$lang['button_checkout'] = 'Paiement';
$lang['button_buying'] = 'Continuer les achats';

/*Alert Lang*/
$lang['alert_successful_purchase'] = 'Article acheté avec succès.';
$lang['alert_upload_error'] = 'Votre image doit être au format jpg ou png';
$lang['alert_changelog_not_found'] = 'Le serveur n\'a pas de journal des modifications pour le moment';
$lang['alert_points_insufficient'] = 'Points insuffisants';

/*Status Lang*/
$lang['offline'] = 'Hors ligne';
$lang['online'] = 'En ligne';

/*Label Lang*/
$lang['label_open'] = 'Ouvert';
$lang['label_closed'] = 'Fermé';

/*Form Label Lang*/
$lang['label_login_info'] = 'Informations de connexion';

/*Input Placeholder Lang*/
$lang['placeholder_username'] = 'Nom d\'utilisateur';
$lang['placeholder_email'] = 'Adresse e-mail';
$lang['placeholder_password'] = 'Mot de passe';
$lang['placeholder_re_password'] = 'Répéter le mot de passe';
$lang['placeholder_current_password'] = 'Mot de passe actuel';
$lang['placeholder_new_password'] = 'Nouveau mot de passe';
$lang['placeholder_new_username'] = 'Nouveau nom d\'utilisateur';
$lang['placeholder_confirm_username'] = 'Confirmer le nouveau nom d\'utilisateur';
$lang['placeholder_new_email'] = 'Nouveau Email';
$lang['placeholder_confirm_email'] = 'Confirmer le nouveau Email';
$lang['placeholder_create_bug_report'] = 'Créer un rapport de bug';
$lang['placeholder_title'] = 'Titre';
$lang['placeholder_type'] = 'Type';
$lang['placeholder_description'] = 'Description';
$lang['placeholder_url'] = 'URL';
$lang['placeholder_uri'] = 'URL conviviale (exemple: tos)';
$lang['placeholder_highl'] = 'Surligner';
$lang['placeholder_lock'] = 'Bloquer';
$lang['placeholder_subject'] = 'Sujet';

/*Table header Lang*/
$lang['table_header_name'] = 'Nom';
$lang['table_header_faction'] = 'Faction';
$lang['table_header_total_kills'] = 'Nombre total de victimes';
$lang['table_header_today_kills'] = 'Victimes aujourd\'hui';
$lang['table_header_yersterday_kills'] = 'Victimes hier';
$lang['table_header_team_name'] = 'Nom de l\'équipe';
$lang['table_header_members'] = 'Membres';
$lang['table_header_rating'] = 'Notation';
$lang['table_header_games'] = 'Jeux';
$lang['table_header_id'] = 'ID';
$lang['table_header_status'] = 'Statut';
$lang['table_header_priority'] = 'Priorité';
$lang['table_header_date'] = 'Date';
$lang['table_header_author'] = 'Autheur';
$lang['table_header_time'] = 'Temps';
$lang['table_header_icon'] = 'Icon';
$lang['table_header_realm'] = 'Royaume';
$lang['table_header_zone'] = 'Zone';
$lang['table_header_character'] = 'Personnage';
$lang['table_header_price'] = 'Prix';
$lang['table_header_item_name'] = 'Nom Item';
$lang['table_header_items'] = 'Item(s)';
$lang['table_header_quantity'] = 'Quantité';

/*Class Lang*/
$lang['class_warrior'] = 'Guerrier';
$lang['class_paladin'] = 'Paladin';
$lang['class_hunter'] = 'Chasseur';
$lang['class_rogue'] = 'Voleur';
$lang['class_priest'] = 'Prêtre';
$lang['class_dk'] = 'Chevalier de la mort';
$lang['class_shamman'] = 'Chaman';
$lang['class_mage'] = 'Mage';
$lang['class_warlock'] = 'Démoniste';
$lang['class_monk'] = 'Moine';
$lang['class_druid'] = 'Druide';
$lang['class_demonhunter'] = 'Chasseur de démon';

/*Faction Lang*/
$lang['faction_alliance'] = 'Alliance';
$lang['faction_horde'] = 'Horde';

/*Gender Lang*/
$lang['gender_male'] = 'Mâle';
$lang['gender_female'] = 'Femelle';

/*Race Lang*/
$lang['race_human'] = 'Humain';
$lang['race_orc'] = 'Orc';
$lang['race_dwarf'] = 'Nain';
$lang['race_night_elf'] = 'Elfe de nuit';
$lang['race_undead'] = 'Mort-vivant';
$lang['race_tauren'] = 'Tauren';
$lang['race_gnome'] = 'Gnome';
$lang['race_troll'] = 'Troll';
$lang['race_goblin'] = 'Gobelin';
$lang['race_blood_elf'] = 'Elfe de sang';
$lang['race_draenei'] = 'Draenei';
$lang['race_worgen'] = 'Worgen';
$lang['race_panda_neutral'] = 'Pandaren Neutre';
$lang['race_panda_alli'] = 'Pandaren Alliance';
$lang['race_panda_horde'] = 'Pandaren Horde';
$lang['race_nightborde'] = 'Sacrenuit ';
$lang['race_void_elf'] = 'Elfe du Vide';
$lang['race_lightforged_draenei'] = 'Draeneï sancteforge';
$lang['race_highmountain_tauren'] = 'Tauren de Haut-Ro';
$lang['race_dark_iron_dwarf'] = 'Nain sombrefer';
$lang['race_maghar_orc'] = 'Orcs Mag\'har';

/*Header Lang*/
$lang['header_cookie_message'] = 'Ce site Web utilise des cookies pour vous garantir une meilleure expérience sur notre site Web. ';
$lang['header_cookie_button'] = 'J\'ai compris!';

/*Footer Lang*/
$lang['footer_rights'] = 'Tous les droits sont réservés.';

/*Page 404 Lang*/
$lang['page_404_title'] = '404 Page non trouvée';
$lang['page_404_description'] = 'Il semble que la page que vous recherchez est introuvable';

/*Panel Lang*/
$lang['panel_acc_rank'] = 'Rang du compte';
$lang['panel_dp'] = 'Points donateurs';
$lang['panel_vp'] = 'Vote Points';
$lang['panel_expansion'] = 'Extension';
$lang['panel_member'] = 'Membre depuis';
$lang['panel_chars_list'] = 'Liste des personnages';
$lang['panel_account_details'] = 'Détails du compte';
$lang['panel_last_ip'] = 'Dernière IP';
$lang['panel_change_email'] = 'Changer l\'adresse email';
$lang['panel_change_username'] = 'Changer le nom d\'utilisateur';
$lang['panel_change_password'] = 'Changer le mot de passe';
$lang['panel_replace_pass_by'] = 'Remplacer le mot de passe par';
$lang['panel_current_username'] = 'Nom d\'utilisateur actuel';
$lang['panel_current_email'] = 'Adresse e-mail actuelle';
$lang['panel_replace_email_by'] = 'Remplacer l\'e-mail par';

/*Home Lang*/
$lang['home_latest_news'] = 'Dernières News';
$lang['home_discord'] = 'Discord';
$lang['home_server_status'] = 'État du serveur';
$lang['home_realm_info'] = 'Actuellement, le royaume est';

/*PvP Statistics Lang*/
$lang['statistics_top_20'] = 'TOP 20';
$lang['statistics_top_2v2'] = 'TOP 2V2';
$lang['statistics_top_3v3'] = 'TOP 3V3';
$lang['statistics_top_5v5'] = 'TOP 5V5';

/*News Lang*/
$lang['news_recent_list'] = 'Liste des news récentes';
$lang['news_comments'] = 'Commentaires';

/*Bugtracker Lang*/
$lang['bugtracker_report_notfound'] = 'Rapports non trouvés';

/*Donate Lang*/
$lang['donate_get'] = 'Obtenir';

/*Vote Lang*/
$lang['vote_next_time'] = 'Prochain vote dans:';

/*Forum Lang*/
$lang['forum_posts_count'] = 'Messages';
$lang['forum_topic_locked'] = 'Ce sujet est verrouillé.';
$lang['forum_comment_locked'] = 'Avez-vous quelque chose à dire? Connecte toi pour rejoindre la conversation.';
$lang['forum_comment_header'] = 'Rejoindre la conversation';
$lang['forum_not_authorized'] = 'Vous n\'êtes pas autorisée';
$lang['forum_post_history'] = 'Afficher l\'historique des publications';
$lang['forum_topic_list'] = 'Liste des sujets';
$lang['forum_last_activity'] = 'Dernère Activité';
$lang['forum_last_post_by'] = 'Dernier message par';
$lang['forum_whos_online'] = 'Qui est en ligne';
$lang['forum_replies_count'] = 'Réponses';
$lang['forum_topics_count'] = 'Sujets';
$lang['forum_users_count'] = 'Utilisateurs';

/*Store Lang*/
$lang['store_categories'] = 'Catégories de la boutique';
$lang['store_top_items'] = 'TOP Items';
$lang['store_cart_added'] = 'Vous avez ajouté';
$lang['store_cart_in_your'] = 'dans votre panier';
$lang['store_cart_no_items'] = 'Vous n\'avez pas d\'articles dans votre panier.';

/*Soap Lang*/
$lang['soap_send_subject'] = 'Achat en boutique';
$lang['soap_send_body'] = 'Merci d\'avoir acheté dans notre boutique!';

/*Email Lang*/
$lang['email_password_recovery'] = 'Récupération de mot de passe';
$lang['email_account_activation'] = 'Activation de compte';
