<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Navbar Lang*/
$lang['admin_nav_dashboard'] = 'Tableau de bord';
$lang['admin_nav_system'] = 'Système';
$lang['admin_nav_manage_settings'] = 'Gérer les paramètres';
$lang['admin_nav_manage_modules'] = 'Gérer les modules';
$lang['admin_nav_users'] = 'Utilisateurs';
$lang['admin_nav_accounts'] = 'Comptes';
$lang['admin_nav_website'] = 'Site Web';
$lang['admin_nav_menu'] = 'Menu';
$lang['admin_nav_realms'] = 'Royaumes';
$lang['admin_nav_slides'] = 'Slides';
$lang['admin_nav_news'] = 'News';
$lang['admin_nav_changelogs'] = 'Changelogs';
$lang['admin_nav_pages'] = 'Pages';
$lang['admin_nav_donate_methods'] = 'Méthodes de donation';
$lang['admin_nav_topsites'] = 'Topsites';
$lang['admin_nav_donate_vote_logs'] = 'Don/Vote Logs';
$lang['admin_nav_store'] = 'Boutique';
$lang['admin_nav_manage_store'] = 'Gérer la boutique';
$lang['admin_nav_forum'] = 'Forum';
$lang['admin_nav_manage_forum'] = 'Gérer le Forum';
$lang['admin_nav_logs'] = 'Système des Logs';

/*Sections Lang*/
$lang['section_general_settings'] = 'Réglages Généraux';
$lang['section_module_settings'] = 'Paramètres des Modules';
$lang['section_optional_settings'] = 'Paramètres Facultatifs';
$lang['section_seo_settings'] = 'Réglages du SEO';
$lang['section_update_cms'] = 'Mettre à jour le CMS';
$lang['section_check_information'] = 'Vérifier les informations';
$lang['section_forum_categories'] = 'Forum Catégories';
$lang['section_forum_elements'] = 'Forum Éléments';
$lang['section_store_categories'] = 'Store Catégories';
$lang['section_store_items'] = 'Items de la Boutique';
$lang['section_store_top'] = 'TOP Items de la Boutique';
$lang['section_logs_dp'] = 'Logs des dons';
$lang['section_logs_vp'] = 'Logs des votes';

/*Button Lang*/
$lang['button_select'] = 'Sélectionner';
$lang['button_update'] = 'Mettre à jour';
$lang['button_unban'] = 'Annuler Ban';
$lang['button_ban'] = 'Ban';
$lang['button_remove'] = 'Retirer';
$lang['button_grant'] = 'Accorder';
$lang['button_update_version'] = 'Mettre à jour vers la dernière version';

/*Table header Lang*/
$lang['table_header_subcategory'] = 'Sélectionnez une sous-catégorie';
$lang['table_header_race'] = 'Race';
$lang['table_header_class'] = 'Classe';
$lang['table_header_level'] = 'Niveau';
$lang['table_header_money'] = 'Argent';
$lang['table_header_time_played'] = 'Temps joué';
$lang['table_header_actions'] = 'Actions';
$lang['table_header_id'] = '#ID';
$lang['table_header_tax'] = 'Tax';
$lang['table_header_points'] = 'Points';
$lang['table_header_type'] = 'Type';
$lang['table_header_module'] = 'Module';
$lang['table_header_payment_id'] = 'Paiement ID';
$lang['table_header_hash'] = 'Hash';
$lang['table_header_total'] = 'Total';
$lang['table_header_create_time'] = 'Créer depuis';
$lang['table_header_guid'] = 'Guid';
$lang['table_header_information'] = 'Information';
$lang['table_header_value'] = 'Valeur';

/*Input Placeholder Lang*/
$lang['placeholder_manage_account'] = 'Gérer le compte';
$lang['placeholder_update_information'] = 'Mise à jour des informations du compte';
$lang['placeholder_donation_logs'] = 'Logs des dons';
$lang['placeholder_store_logs'] = 'Logs de la boutique';
$lang['placeholder_create_changelog'] = 'Créer Changelog';
$lang['placeholder_edit_changelog'] = 'Éditer Changelog';
$lang['placeholder_create_category'] = 'Créer Catégorie';
$lang['placeholder_edit_category'] = 'Éditer Categorie';
$lang['placeholder_create_forum'] = 'Créer Forum';
$lang['placeholder_edit_forum'] = 'Éditer Forum';
$lang['placeholder_create_menu'] = 'Créer Menu';
$lang['placeholder_edit_menu'] = 'Éditer Menu';
$lang['placeholder_create_news'] = 'Créer News';
$lang['placeholder_edit_news'] = 'Éditer News';
$lang['placeholder_create_page'] = 'Créer Page';
$lang['placeholder_edit_page'] = 'Éditer Page';
$lang['placeholder_create_realm'] = 'Créer Royaume';
$lang['placeholder_edit_realm'] = 'Éditer Royaumes';
$lang['placeholder_create_slide'] = 'Créer Slide';
$lang['placeholder_edit_slide'] = 'Éditer Slide';
$lang['placeholder_create_item'] = 'Créer item';
$lang['placeholder_edit_item'] = 'Éditer Item';
$lang['placeholder_create_topsite'] = 'Créer Topsite';
$lang['placeholder_edit_topsite'] = 'Éditer Topsite';
$lang['placeholder_create_top'] = 'Créer TOP Item';
$lang['placeholder_edit_top'] = 'Éditer TOP Item';

$lang['placeholder_upload_image'] = 'Envoyer une image';
$lang['placeholder_icon_name'] = 'Nom de l\'icône';
$lang['placeholder_category'] = 'Catégorie';
$lang['placeholder_name'] = 'Nom';
$lang['placeholder_item'] = 'Item';
$lang['placeholder_image_name'] = 'Nom de l\'image';
$lang['placeholder_reason'] = 'Raison';
$lang['placeholder_gmlevel'] = 'Niveau GM';
$lang['placeholder_url'] = 'URL';
$lang['placeholder_child_menu'] = 'Menu enfant';
$lang['placeholder_url_type'] = 'Type URL';
$lang['placeholder_route'] = 'Route';
$lang['placeholder_hours'] = 'Heures';
$lang['placeholder_soap_hostname'] = 'Nom d\'hôte Soap';
$lang['placeholder_soap_port'] = 'Port Soap';
$lang['placeholder_soap_user'] = 'Utilisateur Soap';
$lang['placeholder_soap_password'] = 'Mot de passe Soap';
$lang['placeholder_db_character'] = 'Character';
$lang['placeholder_db_hostname'] = 'Nom d\'hôte Database';
$lang['placeholder_db_name'] = 'Nom de la base de données';
$lang['placeholder_db_user'] = 'Utilisateur de la base de données';
$lang['placeholder_db_password'] = 'Mot de passe de la base de données';
$lang['placeholder_account_points'] = 'Points du compte';
$lang['placeholder_account_ban'] = 'Ban le compte';
$lang['placeholder_account_unban'] = 'Annuler le bannissement du compte';
$lang['placeholder_account_grant_rank'] = 'Accorder le rang de GM';
$lang['placeholder_account_remove_rank'] = 'Supprimer le rang GM';
$lang['placeholder_command'] = 'Commande';

/*Config Lang*/
$lang['conf_website_name'] = 'Nom du site Web';
$lang['conf_realmlist'] = 'Liste des royaumes';
$lang['conf_discord_invid'] = 'ID d\'invitation Discord';
$lang['conf_timezone'] = 'Timezone';
$lang['conf_theme_name'] = 'Nom du thème';
$lang['conf_maintenance_mode'] = 'Maintenance Mode';
$lang['conf_social_facebook'] = 'Facebook URL';
$lang['conf_social_twitter'] = 'Twitter URL';
$lang['conf_social_youtube'] = 'Youtube URL';
$lang['conf_paypal_currency'] = 'Devise PayPal';
$lang['conf_paypal_mode'] = 'PayPal Mode';
$lang['conf_paypal_client'] = 'PayPal ID Client';
$lang['conf_paypal_secretpass'] = 'Mot de passe secret PayPal';
$lang['conf_default_description'] = 'Description par défaut';
$lang['conf_admin_gmlvl'] = 'Administrateur GMLevel';
$lang['conf_mod_gmlvl'] = 'Moderateur GMLevel';
$lang['conf_recaptcha_key'] = 'Clé du site reCaptcha';
$lang['conf_account_activation'] = 'Activation de compte';
$lang['conf_smtp_hostname'] = 'Nom d\'hôte SMTP';
$lang['conf_smtp_port'] = 'Port SMTP';
$lang['conf_smtp_encryption'] = 'Chiffrement SMTP';
$lang['conf_smtp_username'] = 'Nom d\'utilisateur SMTP';
$lang['conf_smtp_password'] = 'Mot de passe SMTP';
$lang['conf_sender_email'] = 'E-mail de l\'expéditeur';
$lang['conf_sender_name'] = 'Nom de l\'expéditeur';

/*Logs */
$lang['placeholder_logs_dp'] = 'Donation';
$lang['placeholder_logs_quantity'] = 'Quantité';
$lang['placeholder_logs_hash'] = 'Hash';
$lang['placeholder_logs_voteid'] = 'Vote ID';
$lang['placeholder_logs_points'] = 'Points';
$lang['placeholder_logs_lasttime'] = 'Dernière fois';
$lang['placeholder_logs_expiredtime'] = 'Temps écoulé';

/*Status Lang*/
$lang['status_completed'] = 'Terminé';
$lang['status_cancelled'] = 'Annulé';

/*Options Lang*/
$lang['option_normal'] = 'Normal';
$lang['option_dropdown'] = 'Menu déroulant';
$lang['option_image'] = 'Image';
$lang['option_video'] = 'Video';
$lang['option_iframe'] = 'Iframe';
$lang['option_enabled'] = 'Activé';
$lang['option_disabled'] = 'Désactivé';
$lang['option_ssl'] = 'SSL';
$lang['option_tls'] = 'TLS';
$lang['option_everyone'] = 'Tout le monde';
$lang['option_staff'] = 'STAFF';
$lang['option_all'] = 'STAFF - Tout le monde';
$lang['option_rename'] = 'Renommer';
$lang['option_customize'] = 'Personnaliser';
$lang['option_change_faction'] = 'Changer Faction';
$lang['option_change_race'] = 'Changer Race';
$lang['option_dp'] = 'DP';
$lang['option_vp'] = 'VP';
$lang['option_dp_vp'] = 'DP & VP';
$lang['option_internal_url'] = 'URL interne';
$lang['option_external_url'] = 'URL externe';
$lang['option_on'] = 'On';
$lang['option_off'] = 'Off';

/*Count Lang*/
$lang['count_accounts_created'] = 'Comptes créés';
$lang['count_accounts_banned'] = 'Comptes bannis';
$lang['count_news_created'] = 'News créées';
$lang['count_changelogs_created'] = 'Changelogs créés';
$lang['total_accounts_registered'] = 'Total des comptes enregistrés.';
$lang['total_accounts_banned'] = 'Nombre total de comptes bannis.';
$lang['total_news_writed'] = 'Total des news écrites.';
$lang['total_changelogs_writed'] = 'Nombre total de Changelogs écrits.';

$lang['info_alliance_players'] = 'Joueurs Alliance';
$lang['info_alliance_playing'] = 'Alliances jouant sur le royaume';
$lang['info_horde_players'] = 'Joueurs Horde';
$lang['info_horde_playing'] = 'Hordes jouant sur le royaume';
$lang['info_players_playing'] = 'Joueurs jouant sur le royaume';

/*Alert Lang*/
$lang['alert_smtp_activation'] = 'Si vous activez cette option, vous devez configurer un serveur SMTP pour envoyer des e-mails.';
$lang['alert_banned_reason'] = 'Est bannis, la raison:';

/*Logs Lang*/
$lang['log_new_level'] = 'Recevez un nouveau niveau';
$lang['log_old_level'] = 'Avant c\'était';
$lang['log_new_name'] = 'Il a un nouveau nom';
$lang['log_old_name'] = 'Avant c\'était';
$lang['log_unbanned'] = 'Non banni';
$lang['log_customization'] = 'Obtenez une personnalisation';
$lang['log_change_race'] = 'Obtenez un changement de race';
$lang['log_change_faction'] = 'Obtenez un changement de faction';
$lang['log_banned'] = 'A été banni';
$lang['log_gm_assigned'] = 'A reçu le rang de GM';
$lang['log_gm_removed'] = 'Le rang de GM a été supprimé';

/*CMS Lang*/
$lang['cms_version_currently'] = 'Cette version est actuellement en cours d\'exécution';
$lang['cms_warning_update'] = 'Lorsque le cms est mis à jour, la configuration peut être restaurée par défaut en fonction des modifications apportées à chaque version.';
$lang['cms_php_version'] = 'Version PHP';
$lang['cms_allow_fopen'] = 'allow_url_fopen';
$lang['cms_allow_include'] = 'allow_url_include';
$lang['cms_loaded_modules'] = 'Modules chargés';
$lang['cms_loaded_extensions'] = 'Extensions chargées';
