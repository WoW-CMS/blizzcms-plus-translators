<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Browser Tab Menu*/
$lang['tab_news'] = 'Neuigkeiten';
$lang['tab_forum'] = 'Forum';
$lang['tab_store'] = 'Shop';
$lang['tab_faq'] = 'FAQ';
$lang['tab_bugtracker'] = 'Bugtracker';
$lang['tab_changelogs'] = 'Änderungsprotokolle';
$lang['tab_pvp_statistics'] = 'PvP Statistiken';
$lang['tab_login'] = 'Einloggen';
$lang['tab_register'] = 'Registrieren';
$lang['tab_home'] = 'Home';
$lang['tab_donate'] = 'Spenden';
$lang['tab_vote'] = 'Voten';
$lang['tab_cart'] = 'Einkaufswagen';
$lang['tab_account'] = 'Mein Account';
$lang['tab_reset'] = 'Passwort widerherstellen';
$lang['tab_error'] = 'Error 404';

/*Feld Navbar*/
$lang['navbar_vote_panel'] = 'Voten';
$lang['navbar_donate_panel'] = 'Spenden';

/*Button Lang*/
$lang['button_register'] = 'Registrieren';
$lang['button_login'] = 'Einloggen';
$lang['button_logout'] = 'Ausloggen';
$lang['button_forgot_password'] = 'Passwort vergessen?';
$lang['button_user_panel'] = 'Account-Kontrollzentrum';
$lang['button_admin_panel'] = 'Admin-Kontrollzentrum';
$lang['button_change_avatar'] = 'Avatar ändern';
$lang['button_add_personal_info'] = 'Persönliche Informationen hinzufügen';
$lang['button_create_report'] = 'Fehler melden';
$lang['button_new_topic'] = 'Neues Thema';
$lang['button_edit_topic'] = 'Thema ändern';
$lang['button_save_changes'] = 'Änderungen speichern';
$lang['button_cancel'] = 'Abbrechen';
$lang['button_send'] = 'Senden';
$lang['button_buy'] = 'Kaufen';
$lang['button_read_more'] = 'Mehr lesen';
$lang['button_add_reply'] = 'Antwort hinzufügen';
$lang['button_remove'] = 'Entfernen';
$lang['button_create'] = 'Erstellen';
$lang['button_save'] = 'Speichern';
$lang['button_close'] = 'Schließen';
$lang['button_reply'] = 'Antworten';
$lang['button_donate'] = 'Spenden';
$lang['button_account_settings'] = 'Kontoeinstellungen';

/*Alert Lang*/
$lang['alert_successful_purchase'] = 'Gegenstand erfolgreich erworben.';
$lang['alert_upload_error'] = 'Das Bild muss im jpg oder png Format sein';
$lang['alert_changelog_not_found'] = 'Zur Zeit sind keine Änderungsprotokolle vorhanden.';
$lang['alert_faq_not_found'] = 'Keine FAQs gefunden';
$lang['alert_points_insufficient'] = 'Nicht genug Punkte';

/*Status Lang*/
$lang['offline'] = 'Offline';
$lang['online'] = 'Online';

/*Label Lang*/
$lang['label_open'] = 'Offen';
$lang['label_closed'] = 'Geschlossen';

/*Form Label Lang*/
$lang['label_login_info'] = 'Login Informationen';

/*Input Placeholder Lang*/
$lang['placeholder_username'] = 'Benutzername';
$lang['placeholder_email'] = 'Email';
$lang['placeholder_password'] = 'Passwort';
$lang['placeholder_re_password'] = 'Passwort wiederholen';
$lang['placeholder_current_password'] = 'Aktuelles Passwort';
$lang['placeholder_new_password'] = 'Neues Passwort';
$lang['placeholder_new_email'] = 'Neue Email';
$lang['placeholder_confirm_email'] = 'Bestätige die neue Email';
$lang['placeholder_create_bug_report'] = 'Fehler melden';
$lang['placeholder_title'] = 'Überschrift';
$lang['placeholder_type'] = 'Typ';
$lang['placeholder_description'] = 'Beschreibung';
$lang['placeholder_url'] = 'URL';
$lang['placeholder_uri'] = 'Clean URL (Beispiel: tos)';
$lang['placeholder_highl'] = 'Highlight';
$lang['placeholder_lock'] = 'Schließen';
$lang['placeholder_subject'] = 'Thema';

/*Table header Lang*/
$lang['table_header_name'] = 'Name';
$lang['table_header_faction'] = 'Fraktion';
$lang['table_header_total_kills'] = 'Kills';
$lang['table_header_today_kills'] = 'Heutige Kills';
$lang['table_header_yersterday_kills'] = 'Gestrige Kills';
$lang['table_header_team_name'] = 'Teamname';
$lang['table_header_members'] = 'Mitglieder';
$lang['table_header_rating'] = 'Bewertung';
$lang['table_header_games'] = 'Matches';
$lang['table_header_id'] = 'ID';
$lang['table_header_status'] = 'Status';
$lang['table_header_priority'] = 'Priorität';
$lang['table_header_date'] = 'Datum';
$lang['table_header_author'] = 'Autor';
$lang['table_header_time'] = 'Zeit';
$lang['table_header_icon'] = 'Icon';
$lang['table_header_realm'] = 'Realm';

/*Klasse Lang*/
$lang['class_warrior'] = 'Krieger';
$lang['class_paladin'] = 'Paladin';
$lang['class_hunter'] = 'Jäger';
$lang['class_rogue'] = 'Schuke';
$lang['class_priest'] = 'Priester';
$lang['class_dk'] = 'Todesritter';
$lang['class_shamman'] = 'Shamane';
$lang['class_mage'] = 'Magier';
$lang['class_warlock'] = 'Hexenmeister';
$lang['class_monk'] = 'Mönch';
$lang['class_druid'] = 'Druide';
$lang['class_demonhunter'] = 'Dämonenjäger';

/*Faction Lang*/
$lang['faction_alliance'] = 'Allianz';
$lang['faction_horde'] = 'Horde';

/*Gender Lang*/
$lang['gender_male'] = 'Männlich';
$lang['gender_female'] = 'Weiblich';

/*Race Lang*/
$lang['race_human'] = 'Mensch';
$lang['race_orc'] = 'Ork';
$lang['race_dwarf'] = 'Zwerg';
$lang['race_night_elf'] = 'Nachtelfe';
$lang['race_undead'] = 'Untoter';
$lang['race_tauren'] = 'Tauren';
$lang['race_gnome'] = 'Gnome';
$lang['race_troll'] = 'Troll';
$lang['race_goblin'] = 'Goblin';
$lang['race_blood_elf'] = 'Blutelf';
$lang['race_draenei'] = 'Draenei';
$lang['race_worgen'] = 'Worgen';
$lang['race_panda_neutral'] = 'Pandaren Neutral';
$lang['race_panda_alli'] = 'Pandaren Allianz';
$lang['race_panda_horde'] = 'Pandaren Horde';
$lang['race_nightborde'] = 'Nachtgeborener';
$lang['race_void_elf'] = 'Leerenlf';
$lang['race_lightforged_draenei'] = 'Lichtgeschmiedeter Draenei';
$lang['race_highmountain_tauren'] = 'Hochbergtauren';
$lang['race_dark_iron_dwarf'] = 'Dunkeleisenzwerg';
$lang['race_maghar_orc'] = 'Maghar Ork';

/*Header Lang*/
$lang['header_cookie_message'] = 'Diese Webseite benutzt Cookies um dir die beste Erfahrung zu sichern.';
$lang['header_cookie_button'] = 'Verstanden!';

/*Footer Lang*/
$lang['footer_rights'] = 'All Rechte reserviert.';

/*Page 404 Lang*/
$lang['page_404_title'] = '404 Seite nicht gefunden';
$lang['page_404_description'] = 'Es scheint so als könnten wir die gesuchte Seite nicht finden';

/*Feld Lang*/
$lang['panel_acc_rank'] = 'Kontorang';
$lang['panel_dp'] = 'Spendenpunkte';
$lang['panel_vp'] = 'Votepunkte';
$lang['panel_expansion'] = 'Erweiterung';
$lang['panel_member'] = 'Mitglied seit';
$lang['panel_chars_list'] = 'Charakter Liste';
$lang['panel_account_details'] = 'Account Details';
$lang['panel_last_ip'] = 'Letzte IP';
$lang['panel_change_email'] = 'Emailaddresse ändern';
$lang['panel_change_password'] = 'Passwort ändern';
$lang['panel_replace_pass_by'] = 'Passwort ersetzen von';
$lang['panel_current_email'] = 'aktuelle Emailaddresse';
$lang['panel_replace_email_by'] = 'Email ersetzen von';

/*Home Lang*/
$lang['home_latest_news'] = 'Letzte Neuigkeit';
$lang['home_discord'] = 'Discord';
$lang['home_server_status'] = 'Server Status';
$lang['home_realm_info'] = 'Derzeit ist der Realm ';

/*PvP Statistics Lang*/
$lang['statistics_top_20'] = 'TOP 20';
$lang['statistics_top_2v2'] = 'TOP 2V2';
$lang['statistics_top_3v3'] = 'TOP 3V3';
$lang['statistics_top_5v5'] = 'TOP 5V5';

/*News Lang*/
$lang['news_recent_list'] = 'Kürzliche Neuigkeiten';

/*Bugtracker Lang*/
$lang['bugtracker_report_notfound'] = 'Fehlermeldung nicht gefunden';

/*Spende Lang*/
$lang['donate_get'] = 'Erhalten';

/*Wahl Lang*/
$lang['vote_next_time'] = 'Nächste Votes in:';

/*Forum Lang*/
$lang['forum_posts_count'] = 'Beiträge';
$lang['forum_topic_locked'] = 'Dieses Thema wurde geschlossen.';
$lang['forum_comment_locked'] = 'Du willst an der Unterhaltung teilnehmen? Log dich ein und tritt der Unterhaltung bei.';
$lang['forum_comment_header'] = 'Unterhaltung beitreten';
$lang['forum_not_authorized'] = 'Nicht autorisiert';
$lang['forum_post_history'] = 'Beitragsverlauf sehen';
$lang['forum_topic_list'] = 'Themenliste';
$lang['forum_last_activity'] = 'Letzte Aktivität';
$lang['forum_last_post_by'] = 'Letzter Beitrag von';
$lang['forum_whos_online'] = 'Wer ist online?';

/*Store Lang*/
$lang['store_cart_description'] = 'Gegenstand kaufen ';
$lang['store_item_name'] = 'Gegenstandsname';
$lang['store_select_character'] = 'Charakter auswählen';
$lang['store_item_price'] = 'Preis';
$lang['store_all_categories'] = 'Alle Kategorien';
$lang['store_select_categories'] = 'Wähle eine Kategorie';

/*Soap Lang*/
$lang['store_senditem_subject'] = 'Online erwerben';
$lang['store_senditem_text'] = 'Danke, hier ist der Einkauf';

/*Email Lang*/
$lang['email_password_recovery'] = 'Passwort widerherstellen';
$lang['email_account_activation'] = 'Konto aktivieren';

