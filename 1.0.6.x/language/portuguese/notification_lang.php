<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Notification Title Lang*/
$lang['notification_title_success'] = 'Sucesso';
$lang['notification_title_warning'] = 'Aviso';
$lang['notification_title_error'] = 'Error';
$lang['notification_title_info'] = 'Informação';

/*Notification Message (Login/Register) Lang*/
$lang['notification_username_empty'] = 'Nome de usuário está vazio';
$lang['notification_email_empty'] = 'O e-mail está vazio';
$lang['notification_password_empty'] = 'A senha está vazia';
$lang['notification_user_error'] = 'O nome de usuário ou senha está incorreto. Por favor, tente novamente!';
$lang['notification_email_error'] = 'O e-mail ou a senha está incorreta. Por favor, tente novamente!';
$lang['notification_check_email'] = 'O nome de usuário ou e-mail está incorreto. Por favor tente novamente!';
$lang['notification_checking'] = 'Verificando...';
$lang['notification_redirection'] = 'Conectando-se à sua conta...';
$lang['notification_new_account'] = 'Nova conta criada. redirecionando para login...';
$lang['notification_email_sent'] = 'E-mail enviado. Por favor, verifique o seu e-mail...';
$lang['notification_account_activation'] = 'E-mail enviado. Por favor, verifique o seu e-mail para ativar a sua conta.';
$lang['notification_captcha_error'] = 'Por favor, verifique o captcha';
$lang['notification_password_lenght_error'] = 'Comprimento incorreto da senha. por favor, use uma senha entre 5 e 16 caracteres';
$lang['notification_account_already_exist'] = 'Esta conta já existe';
$lang['notification_password_not_match'] = 'As senhas não coincidem';
$lang['notification_same_password'] = 'A senha é a mesma.';
$lang['notification_currentpass_not_match'] = 'A senha antiga não corresponde';
$lang['notification_used_email'] = 'Email em uso';
$lang['notification_email_not_match'] = 'O e-mail não corresponde';
$lang['notification_expansion_not_found'] = 'Expansão não encontrada';
$lang['notification_valid_key'] = 'Conta ativada';
$lang['notification_valid_key_desc'] = 'Agora você pode entrar com sua conta.';
$lang['notification_invalid_key'] = 'A chave de ativação fornecida não é válida.';

/*Notification Message (General) Lang*/
$lang['notification_email_changed'] = 'O e-mail foi alterado.';
$lang['notification_password_changed'] = 'A senha foi alterada.';
$lang['notification_avatar_changed'] = 'O avatar foi mudado.';
$lang['notification_wrong_values'] = 'Os valores estão errados';
$lang['notification_select_type'] = 'Selecione um tipo';
$lang['notification_select_priority'] = 'Selecione uma prioridade';
$lang['notification_select_category'] = 'Selecione uma Categoria';
$lang['notification_select_realm'] = 'Selecione um Reino';
$lang['notification_select_character'] = 'Selecione um Personagem';
$lang['notification_select_item'] = 'Selecione um item';
$lang['notification_report_created'] = 'O relatório foi criado.';
$lang['notification_title_empty'] = 'O título está vazio';
$lang['notification_description_empty'] = 'A descrição está vazia';
$lang['notification_name_empty'] = 'O nome está vazio';
$lang['notification_id_empty'] = 'id está vazio';
$lang['notification_reply_empty'] = 'A resposta está vazia';
$lang['notification_reply_created'] = 'A resposta foi enviada.';
$lang['notification_reply_deleted'] = 'A resposta foi excluída.';
$lang['notification_topic_created'] = 'O tópico foi criado.';
$lang['notification_donation_successful'] = 'A doação foi concluída com sucesso, verifique os seus pontos de doação na sua conta.';
$lang['notification_donation_canceled'] = 'A doação foi cancelada.';
$lang['notification_donation_error'] = 'As informações fornecidas na transação não correspondem.';
$lang['notification_store_chars_error'] = 'Selecione seu personagem em cada item.';
$lang['notification_store_item_insufficient_points'] = 'Você não tem pontos suficientes para comprar.';
$lang['notification_store_item_purchased'] = 'Os itens foram comprados, por favor verifique o seu correio no jogo.';
$lang['notification_store_item_added'] = 'O item selecionado foi adicionado ao seu carrinho.';
$lang['notification_store_item_removed'] = 'O item selecionado foi removido do seu carrinho.';
$lang['notification_store_cart_error'] = 'A atualização do carrinho falhou, por favor tente novamente.';

/*Notification Message (Admin) Lang*/
$lang['notification_changelog_created'] = 'O changelog foi criado.';
$lang['notification_changelog_edited'] = 'Se editou o changelog.';
$lang['notification_changelog_deleted'] = 'O changelog foi apagado.';
$lang['notification_forum_created'] = 'O fórum foi criado.';
$lang['notification_forum_edited'] = 'O fórum foi editado.';
$lang['notification_forum_deleted'] = 'O fórum foi apagado.';
$lang['notification_category_created'] = 'A categoria foi criada.';
$lang['notification_category_edited'] = 'A categoria foi editada.';
$lang['notification_category_deleted'] = 'A categoria foi apagada.';
$lang['notification_menu_created'] = 'O menu foi criado.';
$lang['notification_menu_edited'] = 'O menu foi editado.';
$lang['notification_menu_deleted'] = 'O menu foi apagado.';
$lang['notification_news_deleted'] = 'As notícias foram apagadas.';
$lang['notification_page_created'] = 'A página foi criada.';
$lang['notification_page_edited'] = 'A página foi editada.';
$lang['notification_page_deleted'] = 'A página foi apagada.';
$lang['notification_realm_created'] = 'O reino foi criado.';
$lang['notification_realm_edited'] = 'O reino foi editado.';
$lang['notification_realm_deleted'] = 'O reino foi apagado.';
$lang['notification_slide_created'] = 'O slide foi criado.';
$lang['notification_slide_edited'] = 'O slide foi editado.';
$lang['notification_slide_deleted'] = 'O slide foi apagado.';
$lang['notification_item_created'] = 'O item foi criado.';
$lang['notification_item_edited'] = 'O item foi editado.';
$lang['notification_item_deleted'] = 'O item foi apagado.';
$lang['notification_top_created'] = 'O TOP item foi criado';
$lang['notification_top_edited'] = 'O TOP item foi editado';
$lang['notification_top_deleted'] = 'O TOP Item foi apagado';
$lang['notification_topsite_created'] = 'O topsite foi criado.';
$lang['notification_topsite_edited'] = 'O topsite foi editado.';
$lang['notification_topsite_deleted'] = 'O topsite foi apagado.';

$lang['notification_settings_updated'] = 'As configurações foram atualizadas.';
$lang['notification_module_enabled'] = 'O módulo foi habilitado.';
$lang['notification_module_disabled'] = 'O módulo foi desativado.';
$lang['notification_migration'] = 'As configurações foram definidas.';

$lang['notification_donation_added'] = 'Doação adicionada';
$lang['notification_donation_deleted'] = 'Doação excluída';
$lang['notification_donation_updated'] = 'Doação atualizada';
$lang['notification_points_empty'] = 'Pontos está vazio';
$lang['notification_tax_empty'] = 'A taxa está vazio';
$lang['notification_price_empty'] = 'O preço está vazio';
$lang['notification_incorrect_update'] = 'Atualização inesperada';

$lang['notification_route_inuse'] = 'A rota já está em uso, por favor escolha outra.';

$lang['notification_account_updated'] = 'A conta foi atualizada.';
$lang['notification_dp_vp_empty'] = 'DP/VP está vazio';
$lang['notification_account_banned'] = 'A conta foi banida.';
$lang['notification_reason_empty'] = 'A razão está vazia';
$lang['notification_account_ban_remove'] = 'O banimento da conta foi removido.';
$lang['notification_rank_empty'] = 'Rank está vazio';
$lang['notification_rank_granted'] = 'A Rank foi concedida.';
$lang['notification_rank_removed'] = 'O Rank foi apagado.';

$lang['notification_cms_updated'] = 'O CMS foi atualizado';
$lang['notification_cms_update_error'] = 'O CMS não pôde ser atualizado';
$lang['notification_cms_not_updated'] = 'Uma nova versão não foi encontrada para atualizar';
