<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Browser Tab Menu*/
$lang['tab_news'] = 'Nyheter';
$lang['tab_forum'] = 'Forum';
$lang['tab_store'] = 'Butik';
$lang['tab_bugtracker'] = 'Bugtracker';
$lang['tab_changelogs'] = 'Changelogs';
$lang['tab_pvp_statistics'] = 'PvP-statistik';
$lang['tab_login'] = 'Logga in';
$lang['tab_register'] = 'Registrera';
$lang['tab_home'] = 'Hem';
$lang['tab_donate'] = 'Donera';
$lang['tab_vote'] = 'Rösta';
$lang['tab_cart'] = 'Kundvagn';
$lang['tab_account'] = 'Mitt konto';
$lang['tab_reset'] = 'Återställning av lösenord';
$lang['tab_error'] = 'Fel 404';
$lang['tab_main maintenance'] = 'Underhåll';
$lang['tab_online'] = 'Onlinespelare';

/*Panel Navbar*/
$lang['navbar_vote_panel'] = 'Rösta panelen';
$lang['navbar_donate_panel'] = 'Donera panel';

/*Button Lang*/
$lang['button_register'] = 'Registrera';
$lang['button_login'] = 'Logga in';
$lang['button_logout'] = 'Logga ut';
$lang['button_forgot_password'] = 'Har du glömt ditt lösenord?';
$lang['button_user_panel'] = 'Användarpanelen';
$lang['button_admin_panel'] = 'Adminpanel';
$lang['button_mod_panel'] = 'Modpanel';
$lang['button_change_avatar'] = 'Ändra Avatar';
$lang['button_add_personal_info'] = 'Lägg till personlig information';
$lang['button_create_report'] = 'Skapa rapport';
$lang['button_new_topic'] = 'Nytt ämne';
$lang['button_edit_topic'] = 'Redigera ämne';
$lang['button_save_changes'] = 'Spara ändringar';
$lang['button_cancel'] = 'Avbryt';
$lang['button_send'] = 'Skicka';
$lang['button_read_more'] = 'Läs mer';
$lang['button_add_reply'] = 'Lägg till svar';
$lang['button_remove'] = 'Ta bort';
$lang['button_create'] = 'Skapa';
$lang['button_save'] = 'Spara';
$lang['button_close'] = 'Stäng';
$lang['button_reply'] = 'Svara';
$lang['button_donate'] = 'Donera';
$lang['button_account_settings'] = 'Kontoinställningar';
$lang['button_cart'] = 'Lägg till i varukorgen';
$lang['button_view_cart'] = 'Visa kundvagn';
$lang['button_checkout'] = 'Kassa';
$lang['button_buying'] = 'Fortsätt köpa';

/*Alert Lang*/
$lang['alert_successful_purchase'] = 'Objekt köpt framgångsrikt.';
$lang['alert_upload_error'] = 'Din bild måste vara i jpg- eller png-format';
$lang['alert_changelog_not_found'] = 'Servern har inte ändringsloggar att informera just nu';
$lang['alert_points_insufficient'] = 'Otillräckliga poäng';

/*Status Lang*/
$lang['offline'] = 'Off-line';
$lang['online'] = 'Uppkopplad';

/*Label Lang*/
$lang['label_open'] = 'Öppen';
$lang['label_closed'] = 'Stängt';

/*Form Label Lang*/
$lang['label_login_info'] = 'Inloggningsinformation';

/*Input Placeholder Lang*/
$lang['placeholder_username'] = 'Användarnamn';
$lang['placeholder_email'] = 'E-postadress';
$lang['placeholder_password'] = 'Lösenord';
$lang['placeholder_re_password'] = 'Upprepa lösenord';
$lang['placeholder_current_password'] = 'Aktuellt lösenord';
$lang['placeholder_new_password'] = 'Nytt lösenord';
$lang['placeholder_new_email'] = 'Ny e-post';
$lang['placeholder_confirm_email'] = 'Bekräfta ny e-post';
$lang['placeholder_create_bug_report'] = 'Skapa bugrapport';
$lang['placeholder_title'] = 'Titel';
$lang['placeholder_type'] = 'Typ';
$lang['placeholder_description'] = 'Beskrivning';
$lang['placeholder_url'] = 'URL';
$lang['placeholder_uri'] = 'Vänlig URL (exempel: tos)';
$lang['placeholder_highl'] = 'Markera';
$lang['placeholder_lock'] = 'Lås';
$lang['placeholder_subject'] = 'Ämne';

/*Table header Lang*/
$lang['table_header_name'] = 'Namn';
$lang['table_header_faction'] = 'Fraktion';
$lang['table_header_total_kills'] = 'Totalt mördande';
$lang['table_header_today_kills'] = 'Mördande i dag';
$lang['table_header_yersterday_kills'] = 'Mördande igår';
$lang['table_header_team_name'] = 'Lagnamn';
$lang['table_header_members'] = 'Medlemmar';
$lang['table_header_rating'] = 'Betyg';
$lang['table_header_games'] = 'Spel';
$lang['table_header_id'] = 'ID';
$lang['table_header_status'] = 'Status';
$lang['table_header_priority'] = 'Prioritet';
$lang['table_header_date'] = 'Datum';
$lang['table_header_author'] = 'Författare';
$lang['table_header_time'] = 'Tid';
$lang['table_header_icon'] = 'Ikon';
$lang['table_header_realm'] = 'Realm';
$lang['table_header_zone'] = 'Zon';
$lang['table_header_character'] = 'Karaktär';
$lang['table_header_price'] = 'Pris';
$lang['table_header_item_name'] = 'Artikelnamn';
$lang['table_header_items'] = 'Produkt (er)';
$lang['table_header_quantity'] = 'Kvantitet';

/*Class Lang*/
$lang['class_warrior'] = 'Warrior';
$lang['class_paladin'] = 'Paladin';
$lang['class_hunter'] = 'Hunter';
$lang['class_rogue'] = 'Rogue';
$lang['class_priest'] = 'Priest';
$lang['class_dk'] = 'Death Knight';
$lang['class_shamman'] = 'Shamman';
$lang['class_mage'] = 'Mage';
$lang['class_warlock'] = 'Warlock';
$lang['class_monk'] = 'Monk';
$lang['class_druid'] = 'Druid';
$lang['class_demonhunter'] = 'Demon Hunter';

/*Faction Lang*/
$lang['faction_alliance'] = 'Alliance';
$lang['faction_horde'] = 'Hord';

/*Gender Lang*/
$lang['gender_male'] = 'Man';
$lang['gender_female'] = 'Kvinna';

/*Race Lang*/
$lang['race_human'] = 'Human';
$lang['race_orc'] = 'Orc';
$lang['race_dwarf'] = 'Dwarf';
$lang['race_night_elf'] = 'Night Elf';
$lang['race_undead'] = 'Undead';
$lang['race_tauren'] = 'Tauren';
$lang['race_gnome'] = 'Gnome';
$lang['race_troll'] = 'Troll';
$lang['race_goblin'] = 'Goblin';
$lang['race_blood_elf'] = 'Blood Elf';
$lang['race_draenei'] = 'Draenei';
$lang['race_worgen'] = 'Worgen';
$lang['race_panda_neutral'] = 'Pandaren Neutral';
$lang['race_panda_alli'] = 'Pandaren Alliance';
$lang['race_panda_horde'] = 'Pandaren Horde';
$lang['race_nightborde'] = 'Nightborne';
$lang['race_void_elf'] = 'Void Elf';
$lang['race_lightforged_draenei'] = 'Lightforged Draenei';
$lang['race_highmountain_tauren'] = 'Highmountain Tauren';
$lang['race_dark_iron_dwarf'] = 'Dark Iron Dwarf';
$lang['race_maghar_orc'] = 'Maghar Orc';

/*Header Lang*/
$lang['header_cookie_message'] = 'Denna webbplats använder cookies för att säkerställa att du får den bästa upplevelsen på vår webbplats. ';
$lang['header_cookie_button'] = 'Jag har det!';

/*Footer Lang*/
$lang['footer_rights'] = 'Alla rättigheter reserverade.';

/*Page 404 Lang*/
$lang['page_404_title'] = '404 Sidan hittades inte';
$lang['page_404_description'] = 'Det ser ut som att den sida du letade efter inte kunde hittas';

/*Panel Lang*/
$lang['panel_acc_rank'] = 'Kontorrangering';
$lang['panel_dp'] = 'Donatorpoäng';
$lang['panel_vp'] = 'Rösta poäng';
$lang['panel_expansion'] = 'Expansion';
$lang['panel_member'] = 'Medlem sedan';
$lang['panel_chars_list'] = 'Teckenlista';
$lang['panel_account_details'] = 'Kontodetaljer';
$lang['panel_last_ip'] = 'Sista IP';
$lang['panel_change_email'] = 'Ändra e-postadress';
$lang['panel_change_password'] = 'Ändra lösenord';
$lang['panel_replace_pass_by'] = 'Ersätt lösenord med';
$lang['panel_current_email'] = 'Aktuell e-postadress';
$lang['panel_replace_email_by'] = 'Ersätt e-post med';

/*Home Lang*/
$lang['home_latest_news'] = 'Senaste nyheterna';
$lang['home_discord'] = 'Discord';
$lang['home_server_status'] = 'Serverstatus';
$lang['home_realm_info'] = 'För närvarande är realmen';

/*PvP Statistics Lang*/
$lang['statistics_top_20'] = 'TOPP 20';
$lang['statistics_top_2v2'] = 'TOPP 2V2';
$lang['statistics_top_3v3'] = 'TOPP 3V3';
$lang['statistics_top_5v5'] = 'TOPP 5V5';

/*News Lang*/
$lang['news_recent_list'] = 'Senaste nyhetslista';
$lang['news_comments'] = 'Kommentarer';

/*Bugtracker Lang*/
$lang['bugtracker_report_notfound'] = 'Rapporter hittades inte';

/*Donate Lang*/
$lang['donate_get'] = 'Skaffa';

/*Vote Lang*/
$lang['vote_next_time'] = 'Nästa omröstning i:';

/*Forum Lang*/
$lang['forum_posts_count'] = 'Inlägg';
$lang['forum_topic_locked'] = 'Detta ämne är låst.';
$lang['forum_comment_locked'] = 'Har något att säga? Logga in för att gå med i konversationen. ';
$lang['forum_comment_header'] = 'Gå med i konversationen';
$lang['forum_not_authorized'] = 'Ej godkänt';
$lang['forum_post_history'] = 'Visa inläggshistorik';
$lang['forum_topic_list'] = 'Ämneslista';
$lang['forum_last_activity'] = 'Senaste aktivitet';
$lang['forum_last_post_by'] = 'Sista inlägg av';
$lang['forum_whos_online'] = 'Whos Online';
$lang['forum_replies_count'] = 'Svar';
$lang['forum_topics_count'] = 'Ämnen';
$lang['forum_users_count'] = 'Användare';

/*Store Lang*/
$lang['store_categoryer'] = 'Butikskategorier';
$lang['store_top_items'] = 'TOPP Artiklar';
$lang['store_cart_added'] = 'Du har lagt till';
$lang['store_cart_in_your'] = 'i din kundvagn';
$lang['store_cart_no_items'] = 'Du har inga artiklar i din kundvagn.';

/*Soap Lang*/
$lang['soap_send_subject'] = 'Butiksköp';
$lang['soap_send_body'] = 'Tack för att du köpte i vår butik!';

/*Email Lang*/
$lang['email_password_recovery'] = 'Återställning av lösenord';
$lang['email_account_activation'] = 'Kontoaktivering';
